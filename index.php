<?php
	$pages = array(
		'news' => array(
			'title' => 'Dernières informations',
			'text' => 'Actualités'
		),
		'membres' => array(
			'title' => 'Qui sommes nous ?',
			'text' => 'Membres'
		),
		'presentation' => array(
 			'title' => 'Que faisons nous ?',
 			'text' => 'Présentation'
		),
		'photos' => array(
			'title' => 'Photos de nos évènements',
			'text' => 'Photos'
		),
		'videos' => array(
			'title' => 'Vidéos de nos évènements',
			'text' => 'Vidéos'
		),
		'spectacles' => array(
			'title' => 'Nos prochaines représentations',
			'text' => 'Spectacles'
		),
		'contact' => array(
			'title' => 'Nous joindre',
			'text' => 'Contact'
		)
	);
	$currentPage = isset($_GET['p']) ? (array_key_exists($_GET['p'], $pages) && is_file($_GET['p'].'.inc.html') ? $_GET['p'] : '404') : key($pages);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>CourCirk'Hui</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link href="css/reset.css" rel="stylesheet" />
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<link href="css/global.less" rel="stylesheet/less" type="text/css" />
		<?php
			if(is_file('css/'.$currentPage.'.css')) {
				echo '<link href="css/'.$currentPage.'.css" rel="stylesheet" />';
			}
		?>
		<script src="js/less.min.js" type="text/javascript"></script>
	</head>
	<body>
		<!--[if lt IE 9]>
			<div style="background-color: #fff; width: 900px; height: 400px; border: 1px solid #000; position: absolute; z-index: 999; text-align: center; line-height: 400px; margin: auto;">Votre navigateur n'est pas adapté à ce site. Merci d'utiliser Google Chrome, Firefox, Opera ou Internet Explorer en version 9 minimum.</div>
		<![endif]-->
		<div class="container">
			<div id="menu">
				<ul>
				<?php
					foreach($pages as $page => $data) {
						echo '<li class="'.($page === $currentPage ? 'current' : '').'"><a href="?p='.$page.'" title="'.str_replace('"', '\"', $data['title']).'">'.$data['text'].'</a></li>';
					}
				?>
				</ul>
			</div>
			<div id="wrapper_content">
				<div class="title">
					<h1>CourCirk'hui</h1>
				</div>

				<div id="content">
					<?php 
						if(isset($pages[$currentPage])) { 
							echo '
								<div id="portrait"><img src="http://fakeimg.pl/350x200/?text=Photo" title="Courcirk\'hui" /></div>
								<div class="title"><h2>'.$pages[$currentPage]['text'].'</h2></div>
							';
						}
					?>

					<?php include($currentPage.'.inc.html'); ?>
				</div>
			</div>
		</div>

		<script src="http://code.jquery.com/jquery-latest.js" async="true"></script>
		<script src="js/bootstrap.min.js" async="true"></script>
	</body>
</html>